﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Win32;
using System.Security.AccessControl;

namespace WebisidaCleanupTool
{
	class Program
	{
		private const string PRODUCT_DISPAY_NAME = "Webisida Traffic Exchange Tools";
		static void Main(string[] args)
		{
			Console.Title = "Webisida Cleanup Tool v." + typeof(Program).Assembly.GetName().Version.ToString();
			Console.WriteLine("Поиск записей об установке пакета Webisida Traffic Exchange Tools...");

			int foundCount = 0;
			RegistryKey HKEY_USERS = Registry.Users;
			RegistryKey HKEY_LOCAL_MACHINE = Registry.LocalMachine;
			foreach (string sid in HKEY_USERS.GetSubKeyNames())
			{
				try
				{
					using (RegistryKey products = HKEY_USERS.OpenSubKey(sid + @"\SOFTWARE\Microsoft\Installer\Products", RegistryKeyPermissionCheck.ReadWriteSubTree, RegistryRights.Delete | RegistryRights.ReadKey))
					{
						if (products == null)
							continue;

						foreach (string productCode in products.GetSubKeyNames())
						{
							RegistryKey product = null;
							string productName = null;
							try
							{
								product = products.OpenSubKey(productCode, false);
								if (product == null)
									continue;
								productName = product.GetValue("ProductName", null) as string;
							}
							catch (Exception e)
							{
								Console.ForegroundColor = ConsoleColor.Red;
								Console.Write("Ошибка: ");
								Console.ResetColor();
								Console.WriteLine("не удалось прочитать данные о продукте {0}", productName ?? productCode);
								Console.WriteLine("--> {0}: {1}", e.GetType().Name, e.Message);
								continue;
							}
							finally
							{
								if (product != null)
									product.Close();
							}
							if (PRODUCT_DISPAY_NAME.Equals(productName, StringComparison.Ordinal))
							{
								Console.ForegroundColor = ConsoleColor.Yellow;
								Console.Write("Обнаружена запись об установке. Удаление...");
								foundCount++;
								try
								{
#if ALLOW_DELETE
									products.DeleteSubKeyTree(productCode);
#endif
									Console.ForegroundColor = ConsoleColor.Green;
									Console.Write("   успех");
									Console.ResetColor();
									Console.WriteLine(".");
								}
								catch (Exception e)
								{
									Console.ForegroundColor = ConsoleColor.Red;
									Console.Write("   ошибка");
									Console.ResetColor();
									Console.WriteLine(":\r\n  --> {0}: {1}", e.GetType().Name, e.Message);
								}
							}
						}
					}
				}
				catch (Exception e)
				{
					Console.ForegroundColor = ConsoleColor.Red;
					Console.Write("Ошибка: ");
					Console.ResetColor();
					Console.WriteLine("не удалось открыть ветку пользователя {0}", sid);
					Console.WriteLine("--> {0}: {1}", e.GetType().Name, e.Message);
				}
			}

			foreach (string subKeyName in new string[] {
				@"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall",
				@"SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall"
			})
			{
				RegistryKey uninstall = null;
				try
				{
					uninstall = HKEY_LOCAL_MACHINE.OpenSubKey(subKeyName, RegistryKeyPermissionCheck.ReadWriteSubTree, RegistryRights.Delete | RegistryRights.ReadKey);
					if (uninstall == null)
						continue;

					foreach (string uninstallCode in uninstall.GetSubKeyNames())
					{
						RegistryKey uninstallItem = null;
						string productName = null;
						try
						{
							uninstallItem = uninstall.OpenSubKey(uninstallCode, false);
							if (uninstallItem == null)
								continue;
							productName = uninstallItem.GetValue("DisplayName", null) as string;
						}
						catch (Exception e)
						{
							Console.ForegroundColor = ConsoleColor.Red;
							Console.Write("Ошибка: ");
							Console.ResetColor();
							Console.WriteLine("не удалось прочитать запись деинсталляции {0}", productName ?? uninstallCode);
							Console.WriteLine("--> {0}: {1}", e.GetType().Name, e.Message);
							continue;
						}
						finally
						{
							if (uninstallItem != null)
								uninstallItem.Close();
						}
						if (PRODUCT_DISPAY_NAME.Equals(productName, StringComparison.Ordinal))
						{
							Console.ForegroundColor = ConsoleColor.Yellow;
							Console.Write("Обнаружена запись деинсталляции. Удаление...");
							foundCount++;
							try
							{
#if ALLOW_DELETE
								uninstall.DeleteSubKeyTree(uninstallCode);
#endif
								Console.ForegroundColor = ConsoleColor.Green;
								Console.Write("   успех");
								Console.ResetColor();
								Console.WriteLine(".");
							}
							catch (Exception e)
							{
								Console.ForegroundColor = ConsoleColor.Red;
								Console.Write("   ошибка");
								Console.ResetColor();
								Console.WriteLine(":\r\n  --> {0}: {1}", e.GetType().Name, e.Message);
							}
						}
					}
				}
				catch (Exception e)
				{
					Console.ForegroundColor = ConsoleColor.Red;
					Console.Write("Ошибка: ");
					Console.ResetColor();
					Console.WriteLine("не удалось открыть ветку {0}", subKeyName);
					Console.WriteLine("--> {0}: {1}", e.GetType().Name, e.Message);
				}
				finally
				{
					if (uninstall != null)
						uninstall.Close();
				}
			}

			Console.ResetColor();
			if (foundCount == 0)
			{
				Console.WriteLine("Записи об установке не обнаружены.");
			}
			
			Console.WriteLine();
			Console.WriteLine("Нажмите любую клавишу для выхода из программы...");
			Console.ReadKey();
		}
	}
}
